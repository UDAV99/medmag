<? include_once('_header.php')?>

<div class="h80"></div>
<h1 class="pagetitle text-center">Спасибо за Ваш заказ.</h1>
<div class="container order_done">
  <div class="row">
    <div class="col-12">
      <div class="order_done__number">Номер Вашего заказа  <span>23589</span></div>
      <div class="order_done__text">Мы перезвоним Вам в течение рабочего дня.</div>
    </div>
  </div>
  <div class="row">
    <?
    $x=0;
    while ($x<4) {
      ?>
      <div class="col-md-4 col-xl-3 product_cart<?=$x==3?' temp_hidden':''?>">
        <div class="product_cart__box">
          <div class="product_cart__img"><a href="#!"><img src="img/temp-product.png" alt="" class="img-fluid"></a></div>
          <div class="product_cart__title"><a href="#!">Биохимический анализатор Изи Тач (EasyTouch GCHb)</a></div>
          <div class="product_cart__price">2540</div>
          <div class="product_cart__button"><a href="product.php" class="btn btn-blue btn-sm"><span>Купить</span></a></div>
        </div>
      </div>
      <?
      $x++;
    }
    ?>
  </div>
  <div class="row">
    <div class="col-12 text-center">
      <a href="/" class="btn btn-red btn-sm">На главную</a>
    </div>
  </div>
</div>

<? include_once('_footer.php')?>
