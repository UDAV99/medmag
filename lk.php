<? include_once('_header.php')?>

<div class="container">
  <div class="breadcrumbs">
    <ul>
      <li><a href="/">Главная</a></li>
      <li>Личный кабинет</li>
    </ul>
  </div>
</div>

<h1 class="pagetitle text-center">Личный кабинет</h1>

<div class="container user_cabinet">
  <div class="row">
    <div class="order-2 order-xl-1 col-12 col-xl-6 block-left">
      <div class="white_box userinfo_box">
        <div class="h2">Основная информация</div>
        <form action="" method="post">
          <div class="row_form userinfo">
            <div class="row_form__title">Личная информация</div>
            <input type="text" name="name" placeholder="Имя" value="Надежда">
            <input type="text" name="name" placeholder="Фамилия" value="Тарасова">
            <input type="text" name="name" placeholder="Номер телефона" value="+7 (999) 123-45-67">
          </div>
          <div class="row_form adress">
            <div class="row_form__title">Адрес доставки</div>
            <input type="text" name="name" placeholder="Город">
            <input type="text" name="name" placeholder="Улица">
            <div class="row">
              <div class="col-12 col-md-4 mb20"><input type="text" name="name" placeholder="Номер дома"></div>
              <div class="col-12 col-md-4 mb20"><input type="text" name="name" placeholder="Кв/офис"></div>
              <div class="col-12 col-md-4"><input type="text" name="name" placeholder="Этаж"></div>
            </div>
            <textarea name="name" rows="8" cols="80" placeholder="Комментарий"></textarea>
          </div>

          <div class="text-center">
            <button class="btn btn-red btn-sm">Сохранить</button>
          </div>
        </form>
      </div>
    </div>
    <div class="order-1 order-xl-2 col-12 col-xl-6 block-right">
      <div class="white_box order_history">
        <div class="h2">История заказов</div>
        <div class="link-right"><a href="history.php">Все заказы</a></div>
        <table>
          <tr>
            <th>ID</th>
            <th>Статус</th>
            <th>Дата</th>
            <th>Сумма</th>
          </tr>
          <tr>
            <td>#3</td>
            <td>Открыт</td>
            <td>15/10/2018</td>
            <td>60 000 &#8381;</td>
          </tr>
          <tr>
            <td>#2</td>
            <td>Открыт</td>
            <td>05/10/2018</td>
            <td>50 000 &#8381;</td>
          </tr>
          <tr>
            <td>#1</td>
            <td>Открыт</td>
            <td>21/08/2018</td>
            <td>230 000 &#8381;</td>
          </tr>
        </table>
      </div>
      <div class="white_box social">
        <div class="h2">Социальные сети</div>
        <div class="icon_soc">
          <img src="img/icon-vk.svg" alt="">
          <a href="">Надежда Тарасова</a>
          <a href="#!" class="del">удалить</a>
        </div>
        <div class="icon_soc">
          <img src="img/icon-fb.svg" alt="">
          + <a href="" class="add">добавить</a>
        </div>
        <div class="icon_soc">
          <img src="img/icon-gp.svg" alt="">
          + <a href="" class="add">добавить</a>
        </div>
      </div>
      <div class="white_box password">
        <div class="h2">Изменение пароля</div>
        <form action="" method="post">
          <div class="row_form password">
            <input type="password" name="name" placeholder="Введите старый пароль">
            <input type="password" name="name" placeholder="Введите новый пароль">
            <input type="password" name="name" placeholder="Повторите новый пароль">
          </div>

          <div class="text-center">
            <button class="btn btn-red btn-sm">Сохранить</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<? include_once('_footer.php')?>
