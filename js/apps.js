$(function() {
  $('.actions_index .owl-carousel').owlCarousel({
    loop:true,
    margin:5,
    nav:true,
    dots:false,
    items:1
  });

  $('.partners .owl-carousel').owlCarousel({
    loop:true,
    margin:40,
    nav:true,
    dots:false,
    responsive : {
      0 : {
          items : 2,
          nav:false,
          center:true,
          autoplay:true,
          autoplayTimeout:5000,
          autoplayHoverPause:true
      },
      767 : {
          items : 3,
      },
      1199 : {
          items : 5,
      }
    }
  });

  $('.owl-carousel').owlCarousel({
    loop:true,
    margin:0,
    responsive : {
      0 : {
        items : 1,
        nav:false,
        dots:true,
      },
      767 : {
        items : 3,
        nav:true,
        dots:false,
      },
      1199 : {
        items : 4,
      }
    }
  });

  $('.act_filter').on('click',function(){
    $('form').slideToggle();
    $(this).toggleClass('active');
  });

  $('.dropdown_menu .list').on('click',function(){
    $('.dropdown_select').html($(this).html());
  });

  $(".count_minus").on('click',function(){
    var min = 2,
        inp = $(this).next();
    if(inp.val() >= min) {
      inp.val(Number(inp.val())-1);
    }

  });
  $(".count_plus").on('click',function(){
      var inp = $(this).prev();
      inp.val(Number(inp.val())+1);
  });

  $('.header__mainNavi').on('click', function() {
    $('.hamb_menu').toggleClass('act');
    $('.header_big_n').toggleClass('act');
    $('#dark_bg').toggleClass('act');
    $('.dark_bg_close').toggleClass('act');
  });
  $('#dark_bg, .dark_bg_close').on('click', function() {
    $('.hamb_menu').toggleClass('act');
    $('.header_big_n').toggleClass('act');
    $('.dark_bg_close').toggleClass('act');
    $('#dark_bg').toggleClass('act');
  });

  $('.mobile_catalog').on('click', function() {
    $(this).toggleClass('act');
  });

  $('.header__big_navi > ul > li').on('click', function() {
    $(this).toggleClass('act');
  });


  $('.filter__name').on('click', function() {
    $(this).toggleClass('active');
    $(this).next().slideToggle();
  });

  $( "#slider-range" ).slider({
    range: true,
    min: 1200,
    max: 9540,
    values: [2700, 8300],
    slide: function( event, ui ) {
      $("#range_1").val(ui.values[0]);
      $("#range_2").val(ui.values[1]);
    }
  });
  $("#range_1").val($( "#slider-range" ).slider( "values", 0 ));
  $("#range_2").val($( "#slider-range" ).slider( "values", 1 ));

  $("a.ancLinks").on('click',function() {
    $("#dark_bg").fadeIn(100);
    var h = $('.header').height();
    $("html, body").animate({
      scrollTop: $($(this).attr("href")).offset().top - h + "px"
    }, {
      duration: 700,
      easing: "swing",
      complete: function() {
         $("#dark_bg").fadeOut(200);
       }
    });
    return false;
  });

  $('.divMatch').matchHeight({
    byRow: true,
    property: 'height',
    target: null,
    remove: false
  });

  // MODAL
  $('.btn_modal_forget').on('click', function(){
    $(this).parents('.modal-body').addClass('hide');
    $(this).parents('.modal-content').find('.modal_forget_body').removeClass('hide');
    $('.modal_social').fadeOut(0);
  });

  $('.btn_modal_registered').on('click', function(){
    $(this).parents('.modal-body').addClass('hide');
    $(this).parents('.modal-content').find('.modal_registered_body').removeClass('hide');
    $('.modal_social').fadeOut(0);
  });

  $('.btn_modal_enter').on('click', function(){
    $(this).parents('.modal-body').addClass('hide');
    $(this).parents('.modal-content').find('.modal_enter_body').removeClass('hide');
    $('.modal_social').fadeIn(0);
  });

  $('.modal').on('hidden.bs.modal', function (e) {
    $('.modal .modal-body').each(function(k,v) {
      $('.modal-body').addClass('hide');
      $('.modal-body.first').removeClass('hide');
    });
  });

  $('[name="reception"]').on('change',function() {
    change_reception();
  });
  change_reception();

  $(window).on('resize', function(){
    actionMobile();
  });
  actionMobile();

});

function actionMobile() {
  var win = $(window);

  if (win.width() <= 1199) {
    $('.header__nav').prependTo('.header__big_navi'); // навигация
    $('.header__mainNavi .text').html('Меню');
    $('.userinfo_box').insertAfter('.white_box.order_history');

    $('.row_cart').each(function(k,v) {
      $('.row_cart__price',v).appendTo($('.row_cart__title',v)); // цена
      $('.row_cart__delete',v).appendTo($('.row_cart__addcart',v)); // удалить
      $('.row_cart__price--total',v).prependTo($('.row_cart__delete',v)); // общая цена
    });

    $('.header__big_navi').height($('body').height() - 100);
  }

  if (win.width() > 1199) {
    $('.header__big_navi').height('auto');
    $('.header__nav').insertAfter('.header__logo'); // навигация
    $('.header__mainNavi .text').html('Каталог товаров');
    $('.userinfo_box').appendTo('.user_cabinet .block-left');

    $('.row_cart').each(function(k,v) {
      $('.row_cart__price',v).insertAfter($('.row_cart__title',v)); // цена
      $('.row_cart__delete',v).insertAfter($('.row_cart__addcart',v)); // удалить
      $('.row_cart__price--total',v).insertBefore($('.row_cart__delete',v)); // общая цена
    });
  }

  if (win.width() <= 767) {
    $('.product__favourites').appendTo('.product__addcart');
    $('.inner_product h1').prependTo('.white_box.first');
    $('.footer__addr').appendTo('.footer__mail');

    $('.refresh').addClass('owl-carousel width-sm');
    var owl = $('.refresh.owl-carousel');
    owl.owlCarousel({
      items : 1,
      autoWidth:true,
    });
    $('.refresh.owl-carousel').trigger('refresh.owl.carousel');

    $('.row_cart').each(function(k,v) {
      $('.row_cart__addcart',v).appendTo($('.row_cart__title',v)); // цена
      $('.row_cart__delete',v).insertAfter($('.row_cart__price',v)); // удалить
      $('.row_cart__count',v).insertBefore($('.row_cart__price',v)); // удалить
      $('.row_cart__price .old',v).appendTo($('.row_cart__price',v));
    });
  }
  if (win.width() > 767 && win.width() <= 1199) {
    $('.product__favourites').appendTo('.block-favourites');
    $('.footer__addr').insertAfter('.footer__mail');

    $('.row_cart').each(function(k,v) {
      $('.row_cart__addcart',v).insertAfter($('.row_cart__title',v)); // цена
      $('.row_cart__delete',v).appendTo($('.row_cart__addcart',v)); // удалить
      $('.row_cart__count',v).insertAfter($('.row_cart__title',v)); // удалить
    });

    $('.refresh.owl-carousel').trigger('destroy.owl.carousel').removeClass('owl-carousel');
  }
}

function change_reception() {
  var reception = $('[name="reception"]:radio:checked').val();
  $('.delivery').css('display','none');
  if (reception == 'delivery') {
    $('#order_delivery').css('display','block');
  }
  if (reception == 'pickup') {
    $('#order_pickup').css('display','block');
  }
}
