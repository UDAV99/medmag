<footer>
  <div class="container footer">
    <div class="row row-1">
      <div class="order-2 order-sm-1 col-sm-8 col-xl-9">
        <ul class="footer__navi">
          <li><a href="#!">О компании</a></li>
          <li><a href="#!">Акции</a></li>
          <li><a href="#!">Условия доставки и оплаты</a></li>
          <li><a href="#!">Отзывы</a></li>
          <li><a href="#!">Услуги</a></li>
          <li><a href="#!">Контакты</a></li>
          <li><a href="#!">Наши бренды</a></li>
          <li><a href="#!">Статьи</a></li>
        </ul>
      </div>
      <div class="order-1 order-sm-2 col-sm-4 col-xl-3 footer__block2">
        <div class="order-1 footer__phone">
          <a href="tel:+79262161949">8 (926) 216-19-49</a>
          <a href="tel:+79262161949">8 (926) 216-19-49</a>
        </div>
        <div class="order-4 footer__callback">
          <a href="#!" data-toggle="modal" data-target="#modal_callback">Заказать обратный звонок</a>
        </div>
        <div class="order-2 footer__mail">
          <a href="mailto:shop@site.ru">shop@site.ru</a>
        </div>
        <div class="order-3 footer__addr">
          <div class="addr">ул. Бутырский Вал, д.4</div>
        </div>
      </div>
    </div>
    <div class="row row-2">
      <div class="col-12 col-xl-6">
        <div class="footer__copy--1"><span>MedMag.ru</span> ВОЗМОЖНЫ ПРОТИВОПОКАЗАНИЯ - ПРОКОНСУЛЬТИРУЙТЕСЬ С ВРАЧОМ</div>
        <div class="footer__copy--2">Оперативная доставка по всей территории РФ и в страны СНГ</div>
      </div>
      <div class="col-6 col-xl-3 footer__social">
        <span>Мы в соц. сетях:</span>
        <ul>
          <li><a href=""><svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M28 14C28 6.26801 21.732 0 14 0C6.26801 0 0 6.26801 0 14C0 21.732 6.26801 28 14 28C21.732 28 28 21.732 28 14Z" fill="#919191"/>
          <path d="M14.6316 8.94497C13.6778 8.94941 13.0697 8.95056 12.2123 8.95056C11.4952 8.95056 11.0925 9.24083 11.0797 9.41398C11.068 9.57228 11.4339 9.70029 11.6565 9.82839C11.8713 9.96155 12.0653 10.3843 12.0653 10.6698V13.1605C12.0653 13.3972 12.0115 13.562 11.8203 13.7163C11.7292 13.7898 11.6209 13.8093 11.5039 13.8087C11.4092 13.8082 11.2804 13.7905 11.1917 13.6757C10.2471 12.453 10.244 12.302 8.92082 9.83819C8.85664 9.71869 8.79324 9.5919 8.66181 9.49658C8.53038 9.40126 8.33562 9.33837 8.0234 9.33837C6.95984 9.33837 6.30347 9.35378 5.60414 9.35378C4.94219 9.35378 5.00586 9.75972 5.09452 9.94459C5.88813 11.5994 6.69391 13.1794 7.62019 14.7831C8.70494 16.6612 9.69662 17.7893 11.3653 18.5996C11.7526 18.7877 12.451 18.9818 13.2581 18.9818H14.9732C15.1935 18.9818 15.5598 18.808 15.5598 18.5744V17.2878C15.5598 16.9198 15.9267 16.786 16.1268 16.6956C16.3626 16.589 16.6099 16.757 16.7344 16.8902C17.6932 17.9152 17.5843 17.8036 18.4579 18.7214C18.6553 18.9288 18.8043 19.0378 19.1775 19.0378C21.6971 19.0378 21.6992 19.0426 22.301 19.0378C22.4662 19.0365 22.7116 18.7886 22.7658 18.692C22.8373 18.5645 23.0001 18.1092 22.7392 17.8142C21.8537 16.8129 20.9511 15.8961 20.0861 14.9861C20.0202 14.9167 19.9703 14.8228 19.9685 14.7271C19.9665 14.6224 20.0249 14.5208 20.0861 14.4359C21.0563 13.0893 21.8577 11.949 22.7294 10.5326C23.0127 10.0722 22.9761 9.7461 22.924 9.63378C22.8637 9.50372 22.6779 9.32411 22.4648 9.32297C21.3007 9.31644 20.7972 9.30014 19.5555 9.30197C19.1846 9.30253 18.8561 9.27405 18.6525 9.72058C18.1067 10.9172 17.2163 12.743 16.4432 13.5021C16.3005 13.6422 16.1098 13.7463 15.9434 13.7471C15.777 13.7479 15.6001 13.6345 15.5752 13.3635C15.5562 12.0612 15.572 10.9501 15.5654 9.67438C15.5634 9.28443 15.3989 9.18409 15.2742 9.09757C15.1049 8.9802 14.8375 8.94398 14.6316 8.94497Z" fill="#232323"/>
          </svg></a></li>
          <li><a href=""><svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M12.0237 21.616H15.1709V13.9991H17.2704L17.5485 11.3745H15.1709L15.1741 10.0604C15.1741 9.37627 15.2395 9.009 16.2213 9.009H17.5336V6.384H15.4336C12.9113 6.384 12.0237 7.65753 12.0237 9.7986V11.3745H10.4515V13.9995H12.0237V21.616ZM14 28C6.26827 28 0 21.7317 0 14C0 6.2678 6.26827 0 14 0C21.7317 0 28 6.2678 28 14C28 21.7317 21.7317 28 14 28Z" fill="#919191"/>
          </svg></a></li>
          <li><a href=""><svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M13.9988 16.8027C15.5434 16.8027 16.8027 15.5457 16.8027 14C16.8027 13.3898 16.6031 12.8264 16.2726 12.3663C15.7634 11.6602 14.9355 11.1973 14.0005 11.1973C13.065 11.1973 12.2377 11.6597 11.7274 12.3657C11.3958 12.8259 11.1978 13.3892 11.1973 13.9994C11.1956 15.5451 12.4537 16.8027 13.9988 16.8027Z" fill="#919191"/>
          <path d="M20.1194 10.5713V8.22195V7.87231L19.7681 7.87344L17.4199 7.88077L17.4289 10.5803L20.1194 10.5713Z" fill="#919191"/>
          <path d="M14 0C6.28043 0 0 6.28043 0 14C0 21.719 6.28043 28 14 28C21.719 28 28 21.719 28 14C28 6.28043 21.7201 0 14 0ZM21.9621 12.3663V18.8853C21.9621 20.5833 20.5816 21.9632 18.8847 21.9632H9.11528C7.41787 21.9632 6.03794 20.5833 6.03794 18.8853V12.3663V9.11585C6.03794 7.41843 7.41787 6.03851 9.11528 6.03851H18.8842C20.5816 6.03851 21.9621 7.41843 21.9621 9.11585V12.3663Z" fill="#919191"/>
          <path d="M18.3553 13.9999C18.3553 16.4005 16.4019 18.3551 14.0001 18.3551C11.5984 18.3551 9.6455 16.4005 9.6455 13.9999C9.6455 13.4224 9.76054 12.8704 9.96581 12.3662H7.58887V18.8852C7.58887 19.7277 8.27291 20.41 9.11485 20.41H18.8837C19.7245 20.41 20.4097 19.7277 20.4097 18.8852V12.3662H18.0316C18.2386 12.8704 18.3553 13.4224 18.3553 13.9999Z" fill="#919191"/>
          </svg></a></li>
        </ul>
      </div>
      <div class="col-6 col-xl-3 footer__pay">
        <span>Принимаем к оплате:</span>
        <img src="img/icon-pay.png" alt="">
      </div>
    </div>
  </div>
</footer>



<!-- Modal enter -->
<div class="modal fade" id="modal_enter" tabindex="-1" role="dialog" aria-labelledby="modal_enter" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <div class="modal-body modal_enter_body first">
        <div class="h2">Вход</div>
        <form action="" method="post">
          <div class="row_form">
            <input type="email" name="name" placeholder="Электронная почта">
            <input type="password" name="name" placeholder="Введите пароль">
          </div>
          <div class="text-center popup_footer">
            <button class="btn btn-sm btn-red">Войти</button>
            <a href="#" class="btn_modal_registered">Регистрация</a>
            <a href="#" class="btn_modal_forget">Забыли пароль?</a>
          </div>
        </form>
      </div>
      <div class="modal-body modal_registered_body hide">
        <div class="h2">Регистрация</div>
        <form action="" method="post">
          <div class="row_form">
            <input type="text" name="name" placeholder="Имя">
            <input type="text" name="name" placeholder="Фамилия">
            <input type="text" name="name" placeholder="Введите E-mail">
            <input type="text" name="name" placeholder="Повторите E-mail">
            <input type="text" name="name" placeholder="Номер вашего телефона">
            <input type="password" name="name" placeholder="Введите пароль">
          </div>
          <ul>
            <li><label class="custom"><input type="checkbox" name="name"><span class="checkbox-custom"></span><span class="label">Я прочитал и принимаю условия <a href="">пользовательского соглашения</a></span></label></li>
            <li><label class="custom"><input type="checkbox" name="name" checked><span class="checkbox-custom"></span><span class="label">Хочу получать уведомления о скдиках</span></label></li>
          </ul>

          <div class="text-center popup_footer">
            <button class="btn btn-sm btn-red">Зарегистрироваться</button>
            <a href="#!" class="btn_modal_enter">Уже есть аккаунт?</a>
          </div>
        </form>
      </div>
      <div class="modal-body modal_forget_body hide">
        <div class="h2">Забыли пароль?</div>
        <form action="" method="post">
          <div class="row_form">
            <input type="text" name="name" placeholder="Введите E-mail">
          </div>
          <div class="text-center popup_footer">
            <button class="btn btn-sm btn-red">Восстановить</button>
            <a href="#" class="btn_modal_enter">Назад</a>
            <a href="#" class="btn_modal_registered">Регистрация</a>
          </div>
        </form>
      </div>
      <div class="modal_social">
        <div class="modal_social__text">Или войдите через социальную сеть</div>
        <ul class="modal_social">
          <li><a href=""><svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M28 14C28 6.26801 21.732 0 14 0C6.26801 0 0 6.26801 0 14C0 21.732 6.26801 28 14 28C21.732 28 28 21.732 28 14Z" fill="#ffffff"/>
          <path d="M14.6316 8.94497C13.6778 8.94941 13.0697 8.95056 12.2123 8.95056C11.4952 8.95056 11.0925 9.24083 11.0797 9.41398C11.068 9.57228 11.4339 9.70029 11.6565 9.82839C11.8713 9.96155 12.0653 10.3843 12.0653 10.6698V13.1605C12.0653 13.3972 12.0115 13.562 11.8203 13.7163C11.7292 13.7898 11.6209 13.8093 11.5039 13.8087C11.4092 13.8082 11.2804 13.7905 11.1917 13.6757C10.2471 12.453 10.244 12.302 8.92082 9.83819C8.85664 9.71869 8.79324 9.5919 8.66181 9.49658C8.53038 9.40126 8.33562 9.33837 8.0234 9.33837C6.95984 9.33837 6.30347 9.35378 5.60414 9.35378C4.94219 9.35378 5.00586 9.75972 5.09452 9.94459C5.88813 11.5994 6.69391 13.1794 7.62019 14.7831C8.70494 16.6612 9.69662 17.7893 11.3653 18.5996C11.7526 18.7877 12.451 18.9818 13.2581 18.9818H14.9732C15.1935 18.9818 15.5598 18.808 15.5598 18.5744V17.2878C15.5598 16.9198 15.9267 16.786 16.1268 16.6956C16.3626 16.589 16.6099 16.757 16.7344 16.8902C17.6932 17.9152 17.5843 17.8036 18.4579 18.7214C18.6553 18.9288 18.8043 19.0378 19.1775 19.0378C21.6971 19.0378 21.6992 19.0426 22.301 19.0378C22.4662 19.0365 22.7116 18.7886 22.7658 18.692C22.8373 18.5645 23.0001 18.1092 22.7392 17.8142C21.8537 16.8129 20.9511 15.8961 20.0861 14.9861C20.0202 14.9167 19.9703 14.8228 19.9685 14.7271C19.9665 14.6224 20.0249 14.5208 20.0861 14.4359C21.0563 13.0893 21.8577 11.949 22.7294 10.5326C23.0127 10.0722 22.9761 9.7461 22.924 9.63378C22.8637 9.50372 22.6779 9.32411 22.4648 9.32297C21.3007 9.31644 20.7972 9.30014 19.5555 9.30197C19.1846 9.30253 18.8561 9.27405 18.6525 9.72058C18.1067 10.9172 17.2163 12.743 16.4432 13.5021C16.3005 13.6422 16.1098 13.7463 15.9434 13.7471C15.777 13.7479 15.6001 13.6345 15.5752 13.3635C15.5562 12.0612 15.572 10.9501 15.5654 9.67438C15.5634 9.28443 15.3989 9.18409 15.2742 9.09757C15.1049 8.9802 14.8375 8.94398 14.6316 8.94497Z" fill="#686868"/>
          </svg></a></li>
          <li><a href=""><svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M12.0237 21.616H15.1709V13.9991H17.2704L17.5485 11.3745H15.1709L15.1741 10.0604C15.1741 9.37627 15.2395 9.009 16.2213 9.009H17.5336V6.384H15.4336C12.9113 6.384 12.0237 7.65753 12.0237 9.7986V11.3745H10.4515V13.9995H12.0237V21.616ZM14 28C6.26827 28 0 21.7317 0 14C0 6.2678 6.26827 0 14 0C21.7317 0 28 6.2678 28 14C28 21.7317 21.7317 28 14 28Z" fill="#FFFFFF"/>
          </svg></a></li>
          <li><a href=""><svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M13.9988 16.8027C15.5434 16.8027 16.8027 15.5457 16.8027 14C16.8027 13.3898 16.6031 12.8264 16.2726 12.3663C15.7634 11.6602 14.9355 11.1973 14.0005 11.1973C13.065 11.1973 12.2377 11.6597 11.7274 12.3657C11.3958 12.8259 11.1978 13.3892 11.1973 13.9994C11.1956 15.5451 12.4537 16.8027 13.9988 16.8027Z" fill="#ffffff"/>
          <path d="M20.1194 10.5713V8.22195V7.87231L19.7681 7.87344L17.4199 7.88077L17.4289 10.5803L20.1194 10.5713Z" fill="#ffffff"/>
          <path d="M14 0C6.28043 0 0 6.28043 0 14C0 21.719 6.28043 28 14 28C21.719 28 28 21.719 28 14C28 6.28043 21.7201 0 14 0ZM21.9621 12.3663V18.8853C21.9621 20.5833 20.5816 21.9632 18.8847 21.9632H9.11528C7.41787 21.9632 6.03794 20.5833 6.03794 18.8853V12.3663V9.11585C6.03794 7.41843 7.41787 6.03851 9.11528 6.03851H18.8842C20.5816 6.03851 21.9621 7.41843 21.9621 9.11585V12.3663Z" fill="#ffffff"/>
          <path d="M18.3553 13.9999C18.3553 16.4005 16.4019 18.3551 14.0001 18.3551C11.5984 18.3551 9.6455 16.4005 9.6455 13.9999C9.6455 13.4224 9.76054 12.8704 9.96581 12.3662H7.58887V18.8852C7.58887 19.7277 8.27291 20.41 9.11485 20.41H18.8837C19.7245 20.41 20.4097 19.7277 20.4097 18.8852V12.3662H18.0316C18.2386 12.8704 18.3553 13.4224 18.3553 13.9999Z" fill="#ffffff"/>
          </svg></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<? /*
<!-- Modal registered -->
<div class="modal fade" id="modal_registered" tabindex="-1" role="dialog" aria-labelledby="modal_registered" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <div class="modal-body">
        <div class="h2">Регистрация</div>
        <form action="" method="post">
          <div class="row_form">
            <input type="text" name="name" placeholder="Имя">
            <input type="text" name="name" placeholder="Фамилия">
            <input type="text" name="name" placeholder="Введите E-mail">
            <input type="text" name="name" placeholder="Повторите E-mail">
            <input type="text" name="name" placeholder="Номер вашего телефона">
            <input type="password" name="name" placeholder="Введите пароль">
          </div>
          <ul>
            <li><label class="custom"><input type="checkbox" name="name"><span class="checkbox-custom"></span><span class="label">Я прочитал и принимаю условия <a href="">пользовательского соглашения</a></span></label></li>
            <li><label class="custom"><input type="checkbox" name="name" checked><span class="checkbox-custom"></span><span class="label">Хочу получать уведомления о скдиках</span></label></li>
          </ul>

          <div class="text-center popup_footer">
            <button class="btn btn-sm btn-red">Зарегистрироваться</button>
            <a href="#" data-toggle="modal" data-target="#modal_enter" onclick="$('#modal_registered').modal('hide')">Уже есть аккаунт?</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


<!-- Modal forget -->
<div class="modal fade" id="modal_forget" tabindex="-1" role="dialog" aria-labelledby="modal_forget" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <div class="modal-body">
        <div class="h2">Забыли пароль?</div>
        <form action="" method="post">
          <div class="row_form">
            <input type="text" name="name" placeholder="Введите E-mail или номер телефона">
          </div>
          <div class="text-center popup_footer">
            <button class="btn btn-sm btn-red">Восстановить</button>
            <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modal_enter" onclick="$('#modal_forget').modal('hide')">Назад</a>
            <a href="#" data-toggle="modal" data-target="#modal_registered" onclick="$('#modal_forget').modal('hide')">Регистрация</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
*/?>

<!-- Modal callback -->
<div class="modal fade" id="modal_callback" tabindex="-1" role="dialog" aria-labelledby="modal_callback" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <div class="modal-body">
        <div class="h2">Обратный звонок</div>
        <form action="" method="post">
          <div class="row_form">
            <input type="text" name="name" placeholder="Имя">
            <input type="tel" name="phone" placeholder="Телефон">
          </div>
          <div class="text-center popup_footer">
            <button class="btn btn-sm btn-red">Перезвонить</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal add review -->
<div class="modal fade" id="modal_addreview" tabindex="-1" role="dialog" aria-labelledby="modal_addreview" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <div class="modal-body">
        <div class="h2">Добавить отзыв</div>
        <form action="" method="post">
          <div class="row_form">
            <input type="text" name="name" placeholder="Ваше имя">
            <input type="text" name="name" placeholder="Заголовок">
            <textarea name="name" rows="5" placeholder="Сообщение"></textarea>
          </div>
          <div class="text-center popup_footer">
            <button class="btn btn-sm btn-red">Отправить</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal preorder -->
<div class="modal fade" id="modal_preorder" tabindex="-1" role="dialog" aria-labelledby="modal_preorder" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <div class="modal-body">
        <div class="h2">Оформить предзаказ</div>
        <form action="" method="post">
          <div class="row_form">
            <input type="text" name="name" placeholder="Имя">
            <input type="text" name="name" placeholder="Фамилия">
            <input type="text" name="name" placeholder="Номер вашего телефона">
            <input type="text" name="name" placeholder="Email">
            <textarea name="name" rows="8" placeholder="Комментарий к заказу"></textarea>
          </div>
          <ul>
            <li><label class="custom"><input type="checkbox" name="name"><span class="checkbox-custom"></span><span class="label">Я прочитал и принимаю условия <a href="">пользовательского соглашения</a></span></label></li>
          </ul>

          <div class="text-center popup_footer">
            <button class="btn btn-sm btn-red">Отправить</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div id="dark_bg_modal"></div>
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/owl.sync.js"></script>
<script src="js/jquery.matchHeight-min.js"></script>
<script src="js/apps.js?v=8"></script>
</body>
</html>
