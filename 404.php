<? include_once('_header.php')?>

<div class="error404">
  <div class="error404__img"><img src="img/404.png" alt=""></div>
  <div class="error404__title">Ошибка 404</div>
  <div class="error404__text">Хм, это сбивает с толку.<br>Куда делась страница?!</div>
  <a href="/" class="btn btn-red btn-sm">На главную</a>
</div>

<? include_once('_footer.php')?>
