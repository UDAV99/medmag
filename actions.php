<? include_once('_header.php')?>

<div class="container">
  <div class="breadcrumbs">
    <ul>
      <li><a href="/">Главная</a></li>
      <li>Акции</li>
    </ul>
  </div>
</div>

<h1 class="pagetitle text-center">Акции</h1>

<div class="container">
  <div class="row">
    <?
    $x=0;
    while ($x<12) {
      ?>
      <div class="col-12 col-sm-6 col-xl-4 action_cart">
        <div class="action_cart__box">
          <div class="action_cart__img"><a href="view_action.php"><img src="img/temp-action2.jpg" alt="" class="img-fluid"></a></div>
          <div class="action_cart__title"><a href="view_action.php">Биохимический анализатор Изи Тач (EasyTouch GCHb)</a></div>
        </div>
      </div>
      <?
      $x++;
    }
    ?>
  </div>
  <div class="row">
    <div class="col-12 text-center">
      <!-- <button class="btn btn-red">Загрузить еще</button> -->
      <ul class="paginator">
        <li><a href="#!" class="prev">Предыдущая</a></li>
        <li><a href="#!">1</a></li>
        <li><a href="#!">2</a></li>
        <li><a href="#!" class="active">3</a></li>
        <li><a href="#!">4</a></li>
        <li><a href="#!">5</a></li>
        <li><a href="#!" class="next">Следующая</a></li>
      </ul>
    </div>
  </div>
</div>

<? include_once('_footer.php')?>
