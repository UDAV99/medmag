<? include_once('_header.php')?>

<div class="container">
  <div class="breadcrumbs">
    <ul>
      <li><a href="">Главная</a></li>
      <li><a href="">Тонометры</a></li>
    </ul>
  </div>
</div>

<div class="container inner_product">
  <div class="row row-1">
    <div class="col-12">
      <div class="white_box first">
        <div class="row">
          <div class="col-md-6">
            <div class="product_cart__action view_product">Скидка 60%</div>

            <div id="sync1" class="owl-carousel">
              <div class="product__img text-center"><img src="img/temp-product-big.png" alt=""></div>
              <div class="product__img text-center"><img src="img/temp-product-big.png" alt=""></div>
              <div class="product__img text-center"><img src="img/temp-product-big.png" alt=""></div>
              <div class="product__img text-center"><img src="img/temp-product-big.png" alt=""></div>
              <div class="product__img text-center"><img src="img/temp-product-big.png" alt=""></div>
              <div class="product__img text-center"><img src="img/temp-product-big.png" alt=""></div>
              <div class="product__img text-center"><img src="img/temp-product-big.png" alt=""></div>
              <div class="product__img text-center"><img src="img/temp-product-big.png" alt=""></div>
            </div>
            <div id="sync2" class="owl-carousel d-flex justify-content-center">
              <div class="product__img--sm"><img src="img/temp-product-2.png" alt=""></div>
              <div class="product__img--sm"><img src="img/temp-product-3.png" alt=""></div>
              <div class="product__img--sm"><img src="img/temp-product-4.png" alt=""></div>
              <div class="product__img--sm"><img src="img/temp-product-4.png" alt=""></div>
              <div class="product__img--sm"><img src="img/temp-product-4.png" alt=""></div>
              <div class="product__img--sm"><img src="img/temp-product-4.png" alt=""></div>
              <div class="product__img--sm"><img src="img/temp-product-4.png" alt=""></div>
              <div class="product__img--sm"><img src="img/temp-product-4.png" alt=""></div>
            </div>

          </div>
          <div class="col-md-6">
            <h1>Тонометр YE-660E</h1>
            <div class="product_article"><span>Артикул:</span> PS-004.080</div>
            <div class="product_top_char"><span>Цвет:</span> серый</div>
            <div class="product_top_char"><span>Размер:</span> XL</div>
            <div class="row">
              <div class="col-md-10 col-xl-6">
                <div class="product_instock instock">В наличии</div>
                <div class="product__price--old">3870</div>
                <div class="product__price red">1500</div>
                <div class="product__count">
                  <div class="count_minus">-</div>
                  <input class="count_value" type="number" name="count" value="1" min="1" onkeypress="return event.charCode >= 48">
                  <div class="count_plus">+</div>
                </div>
                <div class="product__addcart">
                  <a href="#" class="btn btn-sm btn-red">В корзину</a>
                </div>
              </div>
              <div class="col-md-2 col-xl-6 d-flex align-items-end block-favourites">
                <a href="#" class="product__favourites">Добавить в избранное</a>
              </div>
            </div>
            <div class="row">
              <div class="col-12 col-xl-6">
                <a href="#" class="btn-grey">Подробнее про условия доставки</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row row-2">
    <div class="col-12">
      <div class="white_box">
        <div class="dropdown_menu">
          <div class="dropdown_select">Характеристики</div>
          <div class="dropdown_list nav nav-tabs">
            <span class="list" data-toggle="tab" href="#tab1">Характеристики</span>
            <span class="list" data-toggle="tab" href="#tab2">Комплектация</span>
            <span class="list" data-toggle="tab" href="#tab3">Отзывы</span>
          </div>
        </div>
        <ul class="nav nav-tabs m-hidden">
          <li><a class="active" data-toggle="tab" href="#tab1">Характеристики</a></li>
          <li><a data-toggle="tab" href="#tab2">Комплектация</a></li>
          <li><a data-toggle="tab" href="#tab3" >Отзывы</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane fade show active" id="tab1">
            <ul>
              <li>Высший класс точности А/А Британского Гипертонического Общества</li>
              <li>Используемые технологии измерения</li>
              <li>Afib – технология выявления главного риска инсульта – мерцательной аритмии</li>
              <li>МАМ - технология 3-х кратного последовательного измерения</li>
              <li>Gentle+ - оптимальная скорость накачки давления в манжету</li>
              <li>Число пользователей - 1</li>
              <li>Память - одно последнее измерение</li>
              <li>Индикация уровня артериального давления</li>
              <li>Индикация правильности надевания манжеты и индикация движения руки</li>
              <li>Сетевой адаптер в комплекте</li>
              <li>Гарантия - 5 лет, бесплатное сервисное обслуживание - 10 лет</li>
            </ul>
          </div>
          <div class="tab-pane fade" id="tab2">.tab3..</div>
          <div class="tab-pane fade" id="tab3">
            <a href="#!" class="add_review top" data-toggle="modal" data-target="#modal_addreview">+ Добавить отзыв</a>
            <?
            $x=0;
            while ($x<6) {
              ?>
              <div class="tab_review_box">
                <div class="name">Олег Петрович</div>
                <div class="desc">
                  <div class="title">Отличная модель!</div>
                  Не следует, однако забывать, что реализация намеченных плановых заданий позволяет выполнять важные задания по разработке направлений прогрессивного развития</div>
              </div>
              <?
              $x++;
            }
            ?>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-sm-4"></div>
              <div class="col-sm-4 text-center"><button class="btn btn-red">Загрузить еще</button></div>
              <div class="col-sm-4 text-right"><a href="#!" class="add_review" data-toggle="modal" data-target="#modal_addreview">+ Добавить отзыв</a></div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="product_also_buy">
  <div class="h2 ">С этим товаром также покупают</div>

  <div class="container">
    <div class="row refresh">
      <?
      $x=0;
      while ($x<4) {
        ?>
        <div class="col-sm-4 col-xl-3 product_cart<?=$x==3?' temp_hidden':''?>">
          <div class="product_cart__box">
            <div class="product_cart__img"><a href="#!"><img src="img/temp-product.png" alt="" class="img-fluid"></a></div>
            <div class="product_cart__title"><a href="#!">Биохимический анализатор Изи Тач (EasyTouch GCHb)</a></div>
            <div class="product_cart__price">2540</div>
            <div class="product_cart__button"><a href="product.php" class="btn btn-blue btn-sm"><span>Купить</span></a></div>
          </div>
        </div>
        <?
        $x++;
      }
      ?>
    </div>
  </div>
</div>

<? include_once('_footer.php')?>
