<? include_once('_header.php')?>

<div class="container">
  <div class="breadcrumbs">
    <ul>
      <li><a href="/">Главная</a></li>
      <li>Оформление заказа</li>
    </ul>
  </div>
</div>

<h1 class="pagetitle text-center">Оформление заказа</h1>

<div class="container step_order_cart">
  <div class="row">
    <div class="order-1 col-xl-6">
      <div class="white_box">
        <div class="h2">Состав заказа</div>
        <div class="edit_order"><a href="">Редактировать заказ</a></div>
        <div class="clearfix"></div>

        <div class="row row_order_cart">
          <div class="col-4 col-sm-2 col-xl-3">
            <a href="#!"><img src="img/temp-product-2.png" alt=""></a>
          </div>
          <div class="col-8 col-sm-10 col-xl-9">
            <div class="row_order_cart__title"><a href="#!">Биохимический анализатор Изи Тач (EasyTouch GCHb)</a></div>
            <div class="row_order_cart__char">Цвет: серый</div>
            <div class="row_order_cart__count">Количество: 1</div>
            <div class="row_order_cart__price">1 х 1500 &#8381; = <span>1500 &#8381;</span></div>
          </div>
        </div>

        <div class="row row_order_cart">
          <div class="col-4 col-sm-2 col-xl-3">
            <a href="#!"><img src="img/temp-product-3.png" alt=""></a>
          </div>
          <div class="col-8 col-sm-10 col-xl-9">
            <div class="row_order_cart__title"><a href="#!">Тонометр автоматический на предплечье MED-53 с адаптером</a></div>
            <div class="row_order_cart__char">Цвет: белый</div>
            <div class="row_order_cart__count">Количество: 1</div>
            <div class="row_order_cart__price">1 х 2100 &#8381; = <span>2100 &#8381;</span></div>
          </div>
        </div>

        <div class="row row_order_cart">
          <div class="col-4 col-sm-2 col-xl-3">
            <a href="#!"><img src="img/temp-product-4.png" alt=""></a>
          </div>
          <div class="col-8 col-sm-10 col-xl-9">
            <div class="row_order_cart__title"><a href="#">Тонометр автоматический на предплечье Microlife BP A150 Afib с адаптером</a></div>
            <div class="row_order_cart__char">Цвет: белый</div>
            <div class="row_order_cart__count">Количество: 1</div>
            <div class="row_order_cart__price">1 х 3426 &#8381; = <span>3426 &#8381;</span></div>
          </div>
        </div>
      </div>
    </div>
    <div class="order-2 col-xl-6">
      <div class="white_box">
        <div class="h2">Основная информация</div>
        <form action="" method="post">
          <div class="row_form">
            <div class="row_form__title">1. Выберите способ получения заказа</div>
            <ul>
              <li><label class="custom"><input type="radio" name="reception" value="delivery"><span class="radio-custom"></span><span class="label">Курьерская доставка</span></label></li>
              <li><label class="custom"><input type="radio" name="reception" value="pickup" checked><span class="radio-custom"></span><span class="label">Самовывоз</span></label></li>
            </ul>
          </div>
          <div class="row_form">
            <div class="row_form__title">2. Введите данные получателя</div>
            <input type="text" name="name" placeholder="Имя">
            <input type="text" name="name" placeholder="Фамилия">
            <input type="text" name="name" placeholder="E-mail">
            <input type="text" name="name" placeholder="Номер телефона">
          </div>
          <div class="row_form delivery" id="order_pickup">
            <div class="row_form__title">3. Выберите пункт выдачи</div>
            <ul>
              <li><label class="custom"><input type="radio" name="name3" checked><span class="radio-custom"></span><span class="label">Галерея Чижова</span></label></li>
              <li><label class="custom"><input type="radio" name="name3"><span class="radio-custom"></span><span class="label">ТРЦ Арена</span></label></li>
            </ul>
          </div>
          <div class="row_form delivery adress" id="order_delivery">
            <div class="row_form__title">3. Введите адрес доставки</div>
            <input type="text" name="name" placeholder="Город">
            <input type="text" name="name" placeholder="Улица">
            <div class="row">
              <div class="col-12 col-md-4 mb20"><input type="text" name="name" placeholder="Номер дома"></div>
              <div class="col-12 col-md-4 mb20"><input type="text" name="name" placeholder="Кв/офис"></div>
              <div class="col-12 col-md-4"><input type="text" name="name" placeholder="Этаж"></div>
            </div>
            <textarea name="name" rows="8" cols="80" placeholder="Комментарий"></textarea>
          </div>
          <div class="row_form">
            <div class="row_form__title">4. Выберите метод оплаты</div>
            <ul>
              <li><label class="custom"><input type="radio" name="name4"><span class="radio-custom"></span><span class="label">Безналичный расчет</span></label></li>
              <li><label class="custom"><input type="radio" name="name4" checked><span class="radio-custom"></span><span class="label">Наличными при получении</span></label></li>
            </ul>
          </div>
          <div class="row_form agree clearfix">
            <div class="left">
              <label class="custom"><input type="checkbox" name="name" checked><span class="checkbox-custom"></span><span class="label">Я принимаю условия</span></label>
              <a href="#!">пользовательского соглашения</a>
            </div>
            <div class="right">
              <div class="sale">Скидка: <span>1074 &#8381;</span></div>
              <div id="total_price">Итого: <span>7026 &#8381;</span></div>
            </div>
          </div>
          <div class="text-center">
            <button class="btn btn-red">Подтвердить заказ</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<? include_once('_footer.php')?>
