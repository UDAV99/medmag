<? include_once('_header.php')?>

<div class="container">
  <div class="breadcrumbs">
    <ul>
      <li><a href="/">Главная</a></li>
      <li>Поиск</li>
      <li>Анализатор</li>
    </ul>
  </div>
</div>

<h2 class="pagetitle text-center">По вашему запросу найдено 25 предметов</h2>
<div class="container search_result">
  <div class="row">
    <div class="col-12">
      <div class="search_result__category">
        <span>В категории:</span>
        <a href="#">Анализаторы</a>, <a href="#">Биохимические анализаторы</a>, <a href="#">Анализаторы EasyTouch</a>
      </div>
    </div>
  </div>
  <div class="row">
    <?
    $x=0;
    while ($x<16) {
      ?>
      <div class="col-sm-4 col-xl-3 product_cart<?=$x==15?' temp_hidden':''?>">
        <div class="product_cart__box">
          <div class="product_cart__img"><a href="#!"><img src="img/temp-product.png" alt="" class="img-fluid"></a></div>
          <div class="product_cart__title"><a href="#!">Биохимический анализатор Изи Тач (EasyTouch GCHb)</a></div>
          <div class="product_cart__price">2540</div>
          <div class="product_cart__button"><a href="product.php" class="btn btn-blue btn-sm"><span>Купить</span></a></div>
        </div>
      </div>
      <?
      $x++;
    }
    ?>
  </div>
  <div class="row">
    <div class="col-12 text-center">
      <ul class="paginator">
        <li><a href="#!" class="prev">Предыдущая</a></li>
        <li><a href="#!">1</a></li>
        <li><a href="#!">2</a></li>
        <li><a href="#!" class="active">3</a></li>
        <li><a href="#!">4</a></li>
        <li><a href="#!">5</a></li>
        <li><a href="#!" class="next">Следующая</a></li>
      </ul>
    </div>
  </div>
</div>

<? include_once('_footer.php')?>
