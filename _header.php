<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <title>МедМаг</title>
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" charset="utf-8">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="css/owl.carousel.min.css" media="screen" charset="utf-8">
  <link rel="stylesheet" href="css/style.css?v=8" media="screen" charset="utf-9">
  <link rel="stylesheet" href="css/media.css?v=8" media="screen" charset="utf-9">
</head>
<body>

<header>
  <div class="container header">
    <div class="row row-1">
      <div class="col-12">
        <div class="header__logo"><a href="/"><img src="img/logo.svg" alt=""></a></div>
        <div class="header__nav clearfix">
          <div class="d-block d-xl-none mobile_title">Меню</div>
          <ul>
            <li class="action"><a href="actions.php">Акции</a></li>
            <li><a href="delivery.php">Доставка и оплата</a></li>
            <li><a href="contact.php">Контакты</a></li>
            <li><a href="about.php">О нас</a></li>
          </ul>
        </div>
        <div class="header__phone">
          <a href="tel:+79262161949">8 (926) 216-19-49</a>
          <a href="tel:+79262161949">8 (926) 216-19-49</a>
        </div>
      </div>
    </div>
    <div class="row row-2">
      <div class="col-12">
        <div class="header__mainNavi">
          <a href="#">
          <div class="hamb_menu">
            <span></span>
            <span></span>
            <span></span>
          </div>
          <span class="text">Каталог товаров</span></a>
        </div>
        <div class="header__search">
          <form class="" action="/search_result.php" method="post">
            <input type="text" name="q" placeholder="Введите поисковый запрос">
            <button class="btn-red">Поиск</button>
          </form>
        </div>
        <div class="header__links">
          <?
          if ($_GET['popup']) {
            ?>
            <ul>
              <li class="header__links--search"><a href="favourites.php">Поиск</a></li>
              <li class="header__links--fav"><a href="favourites.php">Избранное</a></li>
              <li class="header__links--cart"><a href="view_cart.php"><span>8</span>Корзина</a></li>
              <li class="header__links--user no_user"><a href="#!" class="link" data-toggle="modal" data-target="#modal_enter">Вход</a></li>
            </ul>
            <?
          }
          else {
            ?>
            <ul>
              <li class="header__links--search"><a href="favourites.php">Поиск</a></li>
              <li class="header__links--fav"><a href="favourites.php">Избранное</a></li>
              <li class="header__links--cart"><a href="view_cart.php"><span>8</span>Корзина</a></li>
              <li class="header__links--user">
                <a href="lk.php" class="link">Валентина</a>
                <div class="dropdown_menu">
                  <div class="dropdown_list">
                    <span><a href="">Личный кабинет</a></span>
                    <span><a href="">История заказов</a></span>
                    <span><a href="">Выход</a></span>
                  </div>
                </div>
              </li>
            </ul>
            <?
          }
          ?>

        </div>
      </div>
    </div>
  </div>
</header>


<div class="container header_big_n">
  <div class="header__big_navi">
    <div class="d-block d-xl-none mobile_catalog">Каталог продукции</div>
    <ul>
      <li class="subcat"><span class="a">Медицинский компрессионный трикотаж</span>
        <ul>
          <li><a href="">Чулки</a></li>
          <li><a href="">Колготки</a></li>
          <li><a href="">Гольфы</a></li>
          <li><a href="">Для беременных</a></li>
          <li><a href="">Противоэмболические</a></li>
          <li><a href="">Аксессуары и уход</a></li>
        </ul>
      </li>
      <li><a href="">Ортопедическая и комфортная обувь</a></li>
      <li><a href="">Ортопедические стельки</a></li>
      <li><a href="">Индивидуальные ортопедические стельки</a></li>
      <li><a href="">Бандажи</a></li>
      <li><a href="">Ортезы</a></li>
      <li><a href="">Корсеты и корректоры осанки</a></li>
      <li><a href="">Ортопедические подушки и матрасы</a></li>
      <li><a href="">Реабилитация и уход</a></li>
      <li><a href="">Мамология и экзопротезы</a></li>
      <li><a href="">Бытовая медицинская тахника</a></li>
      <li><a href="">Товары для операционного периода</a></li>
      <li><a href="">Товары для медицинских учреждений</a></li>
      <li><a href="">Массажеры и спортивный инвентарь</a></li>
      <li><a href="">Медицинская одежда</a></li>
    </ul>
  </div>
</div>

<div id="dark_bg"></div>
<div class="dark_bg_close"></div>
