<? include_once('_header.php')?>

<div class="container">
  <div class="breadcrumbs">
    <ul>
      <li><a href="">Главная</a></li>
      <li>Глюкометры</li>
    </ul>
  </div>
</div>

<h1 class="pagetitle text-center">Глюкометры</h1>

<div class="container">
  <div class="row">
    <div class="col-md-4 col-xl-3">
      <div class="white_box filter">
        <div class="act_filter d-block d-md-none">Фильтры</div>
        <form action="" method="post">
          <div class="filter__row">
            <div class="filter__name active">Цена</div>
            <div class="filter__params filter__params--price">
              <input type="text" name="" value="" id="range_1" placeholder="От">
              <input type="text" name="" value="" id="range_2" placeholder="До">
              <div class="clearfix"></div>
              <div id="slider-range"></div>
              <div class="box d-flex justify-content-between">
                <div class="min_price">1200</div>
                <div class="r"></div>
                <div class="max_price">9540</div>
              </div>
            </div>
          </div>
          <div class="filter__row">
            <div class="filter__name active">Производитель</div>
            <div class="filter__params">
              <ul>
                <li><label class="custom"><input type="checkbox" name="name" value="" checked><span class="checkbox-custom"></span><span class="label">Accu-Chek / Roche</span></label></li>
                <li><label class="custom"><input type="checkbox" name="name" value="" checked><span class="checkbox-custom"></span><span class="label">LifeScan</span></label></li>
                <li><label class="custom"><input type="checkbox" name="name" value=""><span class="checkbox-custom"></span><span class="label">Сателлит / Элта</span></label></li>
                <li><label class="custom"><input type="checkbox" name="name" value="" checked><span class="checkbox-custom"></span><span class="label">Glucocard / Arkray</span></label></li>
                <li><label class="custom"><input type="checkbox" name="name" value=""><span class="checkbox-custom"></span><span class="label">Clever Chek / Tai Doc</span></label></li>
                <li><label class="custom"><input type="checkbox" name="name" value=""><span class="checkbox-custom"></span><span class="label">Клевер Чек СКС / Осирис С</span></label></li>
                <li><label class="custom"><input type="checkbox" name="name" value="" checked><span class="checkbox-custom"></span><span class="label">Diacont / OK Biotech</span></label></li>
                <li><label class="custom"><input type="checkbox" name="name" value=""><span class="checkbox-custom"></span><span class="label">Sensocard / 77 Elektronika</span></label></li>
                <li><label class="custom"><input type="checkbox" name="name" value="" checked><span class="checkbox-custom"></span><span class="label">iCheck / Diamedical</span></label></li>
                <li><label class="custom"><input type="checkbox" name="name" value=""><span class="checkbox-custom"></span><span class="label">CareSens / I-SENS</span></label></li>
                <li><label class="custom"><input type="checkbox" name="name" value=""><span class="checkbox-custom"></span><span class="label">eBsensor / Visgeneer</span></label></li>
              </ul>
            </div>
          </div>
          <div class="filter__row text-center">
            <button class="btn btn-sm btn-red w-100">Применить</button>
            <a href="#!" class="clear" id="clear_filter">Очистить фильтр</a>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-8 col-xl-9 product_category">
      <div class="row before_category">
        <div class="col-8"><label class="custom"><input type="checkbox" name="name" value=""><span class="checkbox-custom"></span><span class="label">Акционные товары</span></label></div>
        <div class="col-4 text-right">
          <span class="d-none d-md-inline">Сортировать по:</span>
          <div class="dropdown_menu">
            <div class="dropdown_select">Цене</div>
            <div class="dropdown_list">
              <span>Цене</span>
              <span>Популярности</span>
              <span>Просмотрам</span>
              <span>Дате добавления</span>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <?
        $x=0;
        while ($x<12) {
          ?>
          <div class="col-md-6 col-xl-4 product_cart mb30">
            <div class="product_cart__box">
              <div class="product_cart__img"><a href="product.php"><img src="img/temp-product.png" alt="" class="img-fluid"></a></div>
              <div class="product_cart__title"><a href="product.php">Биохимический анализатор Изи Тач (EasyTouch GCHb)</a></div>
              <div class="product_cart__price">2540</div>
              <div class="product_cart__button"><a href="product.php" class="btn btn-blue btn-sm"><span>Купить</span></a></div>
            </div>
          </div>
          <?
          $x++;
        }
        ?>
      </div>
      <div class="row">
        <div class="col-12 text-center">
          <!-- <button class="btn btn-red">Загрузить еще</button> -->
          <ul class="paginator">
            <li><a href="#!" class="prev">Предыдущая</a></li>
            <li><a href="#!">1</a></li>
            <li><a href="#!">2</a></li>
            <li><a href="#!" class="active">3</a></li>
            <li><a href="#!">4</a></li>
            <li><a href="#!">5</a></li>
            <li><a href="#!" class="next">Следующая</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<? include_once('_footer.php')?>
