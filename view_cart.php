<? include_once('_header.php')?>

<div class="container">
  <div class="breadcrumbs">
    <ul>
      <li><a href="/">Главная</a></li>
      <li>Корзина</li>
    </ul>
  </div>
</div>

<h1 class="pagetitle text-center">Корзина</h1>

<div class="container view_cart">
  <div class="row">
    <div class="col-12">
      <div class="white_box">
        <div class="view_cart__row row_cart product d-flex justify-content-between align-items-center">
          <div class="row_cart__img"><a href="#!"><img src="img/temp-product-2.png" alt=""></a></div>
          <div class="row_cart__title">
            <a href="#!">Биохимический анализатор Изи Тач (EasyTouch GCHb)</a>
            <div class="row_cart__art">Артикул: PS-004.080</div>
          </div>
          <div class="row_cart__price">3426</div>
          <div class="row_cart__count product__count">
            <div class="count_minus">-</div>
            <input class="count_value" type="number" name="count" value="1" min="1" onkeypress="return event.charCode >= 48">
            <div class="count_plus">+</div>
          </div>
          <div class="row_cart__price--total">3426</div>
          <div class="row_cart__delete"><a href=""><img src="img/icon-waste-bin.svg" width="16" alt=""></a></div>
        </div>

        <div class="view_cart__row row_cart product d-flex justify-content-between align-items-center">
          <div class="row_cart__img"><a href="#!"><img src="img/temp-product-3.png" alt=""></a></div>
          <div class="row_cart__title">
            <div class="label__red">Скидка 60%</div>
            <div class="label__green">Новинка</div>
            <a href="#!">Тонометр автоматический на предплечье MED-53 с адаптером</a>
            <div class="row_cart__art">Артикул: PS-004.080</div>
          </div>
          <div class="row_cart__price red"><span class="old">4500</span>3426</div>
          <div class="row_cart__count product__count">
            <div class="count_minus">-</div>
            <input class="count_value" type="number" name="count" value="1" min="1" onkeypress="return event.charCode >= 48">
            <div class="count_plus">+</div>
          </div>
          <div class="row_cart__price--total">3426</div>
          <div class="row_cart__delete"><a href=""><img src="img/icon-waste-bin.svg" width="16" alt=""></a></a></div>
        </div>

        <div class="view_cart__row row_cart product d-flex justify-content-between align-items-center">
          <div class="row_cart__img"><a href="#!"><img src="img/temp-product-4.png" alt=""></a></div>
          <div class="row_cart__title">
            <a href="#!">Тонометр автоматический на предплечье Microlife BP A150 Afib с адаптером</a>
            <div class="row_cart__art">Артикул: PS-004.080</div>
          </div>
          <div class="row_cart__price">3426</div>
          <div class="row_cart__count product__count">
            <div class="count_minus">-</div>
            <input class="count_value" type="number" name="count" value="1" min="1" onkeypress="return event.charCode >= 48">
            <div class="count_plus">+</div>
          </div>
          <div class="row_cart__price--total">3426</div>
          <div class="row_cart__delete"><a href=""><img src="img/icon-waste-bin.svg" width="16" alt=""></a></a></div>
        </div>

        <div class="text-center cart_total--sale">Скидка: <span>1074 &#8381;</span></div>
        <div class="text-center cart_total">Итого: <span>7026 &#8381;</span></div>
        <div class="text-center row_btn"><button class="btn btn-red text-uppercase">Оформить заказ</button></div>

      </div>
    </div>
  </div>
</div>

<? include_once('_footer.php')?>
