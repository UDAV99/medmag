<? include_once('_header.php')?>

<div class="container actions_index">
  <div class="row">
    <div class="col-xl-8 actions_index__left">
      <div class="owl-carousel">
        <div class="box">
          <!-- <div class="actions_index__left--img"><img src="img/slider_11.jpg" alt="" class="img-fluid"></div> -->
          <div class="actions_index__left--img" style="background: #BDE0F0 url(img/slider_1.png) no-repeat 70px 50%;">
            <div class="text_box">
              <div class="title">Как выбрать термометр и сэкономить на этом?</div>
              <div class="button">
                <a href="#!">Принять участие</a>
              </div>
            </div>
          </div>
        </div>
        <div class="box">
          <div class="actions_index__left--img" style="background: #BDE0F0 url(img/slider_1.png) no-repeat 70px 50%;">
            <div class="text_box">
              <div class="title">Как выбрать термометр и сэкономить на этом?</div>
              <div class="button">
                <a href="#!">Принять участие</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-4 actions_index__right">
      <div class="row">
        <div class="col-sm-6 col-xl-12">
          <a href="#">
            <div class="box">
              <div class="actions_index__right--img" style="background: #DBDEF1 url(img/slider_2.png) no-repeat;"></div>
              <div class="actions_index__right--title color">Как пройти весь курс лечения без ошибок?</div>
            </div>
          </a>
        </div>
        <div class="col-sm-6 col-xl-12">
          <a href="#">
            <div class="box">
              <div class="actions_index__right--img" style="background: #DDD4C7 url(img/slider_3.png) no-repeat;"></div>
              <div class="actions_index__right--title">С какими препоратами шутки плохи?</div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container index_popular">
  <div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4 h1">Популярное</div>
    <div class="col-md-4 link_right"><a href="#!">Каталог</a></div>
  </div>
  <div class="row product_rows refresh">
      <div class="col-sm-4 col-xl-3 product_cart">
        <div class="product_cart__box">
          <div class="product_cart__img"><a href="#!"><img src="img/temp-product.png" alt="" class="img-fluid"></a></div>
          <div class="product_cart__title divMatch"><a href="#!">Медицинский компрессионный трикотаж</a></div>
          <div class="product_cart__button category"><a href="product.php" class="btn btn-blue btn-sm">В категорию</a></div>
        </div>
      </div>

      <div class="col-sm-4 col-xl-3 product_cart">
        <div class="product_cart__box">
          <div class="product_cart__img"><a href="#!"><img src="img/temp-product.png" alt="" class="img-fluid"></a></div>
          <div class="product_cart__title divMatch"><a href="#!">Ортопедическая и комфортная обувь</a></div>
          <div class="product_cart__button category"><a href="product.php" class="btn btn-blue btn-sm">В категорию</a></div>
        </div>
      </div>

      <div class="col-sm-4 col-xl-3 product_cart">
        <div class="product_cart__box">
          <div class="product_cart__img"><a href="#!"><img src="img/temp-product.png" alt="" class="img-fluid"></a></div>
          <div class="product_cart__title divMatch"><a href="#!">Ортопедические стельки</a></div>
          <div class="product_cart__button category"><a href="product.php" class="btn btn-blue btn-sm">В категорию</a></div>
        </div>
      </div>

      <div class="col-sm-4 col-xl-3 product_cart">
        <div class="product_cart__box">
          <div class="product_cart__img"><a href="#!"><img src="img/temp-product.png" alt="" class="img-fluid"></a></div>
          <div class="product_cart__title divMatch"><a href="#!">Индивидуальные ортопедические стельки</a></div>
          <div class="product_cart__button category"><a href="product.php" class="btn btn-blue btn-sm">В категорию</a></div>
        </div>
      </div>

      <div class="col-sm-4 col-xl-3 product_cart">
        <div class="product_cart__box">
          <div class="product_cart__img"><a href="#!"><img src="img/temp-product.png" alt="" class="img-fluid"></a></div>
          <div class="product_cart__title divMatch"><a href="#!">Бандажи</a></div>
          <div class="product_cart__button category"><a href="product.php" class="btn btn-blue btn-sm">В категорию</a></div>
        </div>
      </div>

      <div class="col-sm-4 col-xl-3 product_cart">
        <div class="product_cart__box">
          <div class="product_cart__img"><a href="#!"><img src="img/temp-product.png" alt="" class="img-fluid"></a></div>
          <div class="product_cart__title divMatch"><a href="#!">Ортезы</a></div>
          <div class="product_cart__button category"><a href="product.php" class="btn btn-blue btn-sm">В категорию</a></div>
        </div>
      </div>

      <div class="col-sm-4 col-xl-3 product_cart temp_hidden">
        <div class="product_cart__box">
          <div class="product_cart__img"><a href="#!"><img src="img/temp-product.png" alt="" class="img-fluid"></a></div>
          <div class="product_cart__title divMatch"><a href="#!">Корсеты и корректоры осанки</a></div>
          <div class="product_cart__button category"><a href="product.php" class="btn btn-blue btn-sm">В категорию</a></div>
        </div>
      </div>

      <div class="col-sm-4 col-xl-3 product_cart temp_hidden">
        <div class="product_cart__box">
          <div class="product_cart__img"><a href="#!"><img src="img/temp-product.png" alt="" class="img-fluid"></a></div>
          <div class="product_cart__title divMatch"><a href="#!">Ортопедические подушки и матрасы</a></div>
          <div class="product_cart__button category"><a href="product.php" class="btn btn-blue btn-sm">В категорию</a></div>
        </div>
      </div>

  </div>
</div>

<div class="container index_sale">
  <div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4 h1 red">Скидки</div>
    <div class="col-md-4 link_right"><a href="#!">Все акционные товары</a></div>
  </div>
  <div class="row refresh">
    <?
    $x=0;
    while ($x<4) {
      ?>
      <div class="col-12 col-sm-4 col-xl-3 product_cart<?=$x==30?' temp_hidden':''?>">
        <div class="product_cart__box">
          <div class="product_cart__action">Скидка 60%</div>
          <div class="product_cart__img"><a href="#!"><img src="img/temp-product.png" alt="" class="img-fluid"></a></div>
          <div class="product_cart__title"><a href="#!">Биохимический анализатор Изи Тач (EasyTouch GCHb)</a></div>
          <div class="product_cart__price red">2540</div>
          <div class="product_cart__price--old">3870</div>
          <div class="product_cart__button"><a href="product.php" class="btn btn-red btn-sm"><span>Купить</span></a></div>
        </div>
      </div>
      <?
      $x++;
    }
    ?>
  </div>
</div>

<div class="container partners">
  <div class="h1">Наши партнеры</div>
  <div class="row">
    <div class="owl-carousel">
      <div class="col-12"><img src="img/partner-logo-1.png" alt=""></div>
      <div class="col-12"><img src="img/partner-logo-2.png" alt=""></div>
      <div class="col-12"><img src="img/partner-logo-3.png" alt=""></div>
      <div class="col-12"><img src="img/partner-logo-4.png" alt=""></div>
      <div class="col-12"><img src="img/partner-logo-5.png" alt=""></div>
      <div class="col-12"><img src="img/partner-logo-1.png" alt=""></div>
      <div class="col-12"><img src="img/partner-logo-2.png" alt=""></div>
      <div class="col-12"><img src="img/partner-logo-3.png" alt=""></div>
      <div class="col-12"><img src="img/partner-logo-4.png" alt=""></div>
      <div class="col-12"><img src="img/partner-logo-5.png" alt=""></div>
    </div>
  </div>
</div>

<? include_once('_footer.php')?>
