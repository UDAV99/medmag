<? include_once('_header.php')?>

<div class="container">
  <div class="breadcrumbs">
    <ul>
      <li><a href="/">Главная</a></li>
      <li>Избранные товары</li>
    </ul>
  </div>
</div>

<h1 class="pagetitle text-center">Избранные товары</h1>

<div class="container view_cart">
  <div class="row">
    <div class="col-12">
      <div class="white_box">
        <div class="view_cart__row row_cart product d-flex justify-content-between align-items-center">
          <div class="row_cart__img"><a href="#!"><img src="img/temp-product-2.png" alt=""></a></div>
          <div class="row_cart__title">
            <a href="#!">Биохимический анализатор Изи Тач (EasyTouch GCHb)</a>
            <div class="row_cart__art">Артикул: PS-004.080</div>
          </div>
          <div class="row_cart__price">3426</div>
          <div class="row_cart__addcart">
            <a href="#!" class="btn btn-blue btn-sm">Добавить в корзину</a>
          </div>
          <div class="row_cart__delete"><a href="">Удалить</a></div>
        </div>

        <div class="view_cart__row row_cart product d-flex justify-content-between align-items-center">
          <div class="row_cart__img"><a href="#!"><img src="img/temp-product-3.png" alt=""></a></div>
          <div class="row_cart__title">
            <div class="label">
              <div class="label__red">Скидка 60%</div>
              <div class="label__green">Новинка</div>
            </div>
            <a href="#!">Тонометр автоматический на предплечье MED-53 с адаптером</a>
            <div class="row_cart__art">Артикул: PS-004.080</div>
          </div>
          <div class="row_cart__price red"><span class="old">4500</span>3426</div>
          <div class="row_cart__addcart">
            <a href="#!" class="btn btn-blue btn-sm">Добавить в корзину</a>
          </div>
          <div class="row_cart__delete"><a href="">Удалить</a></div>
        </div>

        <div class="view_cart__row row_cart product d-flex justify-content-between align-items-center">
          <div class="row_cart__img"><a href="#!"><img src="img/temp-product-4.png" alt=""></a></div>
          <div class="row_cart__title">
            <a href="#!">Тонометр автоматический на предплечье Microlife BP A150 Afib с адаптером</a>
            <div class="row_cart__art">Артикул: PS-004.080</div>
          </div>
          <div class="row_cart__price noitem">Нет в наличии</div>
          <div class="row_cart__addcart">
            <a href="#!" class="btn btn-grey btn-sm" data-toggle="modal" data-target="#modal_preorder">Предзаказ</a>
          </div>
          <div class="row_cart__delete"><a href="">Удалить</a></div>
        </div>

        <div class="row">
          <div class="col-12 text-center">
            <ul class="paginator">
              <li><a href="#!" class="prev">Предыдущая</a></li>
              <li><a href="#!">1</a></li>
              <li><a href="#!">2</a></li>
              <li><a href="#!" class="active">3</a></li>
              <li><a href="#!">4</a></li>
              <li><a href="#!">5</a></li>
              <li><a href="#!" class="next">Следующая</a></li>
            </ul>
          </div>
        </div>


      </div>
    </div>
  </div>
</div>

<? include_once('_footer.php')?>
