<? include_once('_header.php')?>

<div class="container">
  <div class="breadcrumbs">
    <ul>
      <li><a href="/">Главная</a></li>
      <li>История заказов</li>
    </ul>
  </div>
</div>

<h1 class="pagetitle text-center">История заказов</h1>

<div class="container">
  <div class="row">
    <?
    $x=0;
    while ($x<12) {
      ?>
      <div class="col-12 col-md-4 mb">
        <div class="white_box history">
          <div class="history__number">№: 165484522123</div>
          <div class="history__detail">
            <div class="char">
              <span class="label">Дата:</span>
              <span class="value">24.08.2017</span>
            </div>
            <div class="char">
              <span class="label">Стоимость:</span>
              <span class="value">250 500 &#8381;</span>
            </div>
            <div class="char">
              <span class="label">Доставка:</span>
              <span class="value">Доставка курьером</span>
            </div>
            <div class="char">
              <span class="label">Оплата:</span>
              <span class="value">Оплата онлайн</span>
            </div>
            <div class="char">
              <span class="label">Статус:</span>
              <span class="value">Принят в обработку</span>
            </div>
          </div>
          <div class="history__footer">
            <a href="#" class="btn btn-red btn-sm">Повторить</a>
            <a href="#" class="print">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="19" height="18" viewBox="0 0 19 18">
                    <defs>
                      <path id="tic5a" d="M492 522c0-1.1.9-2 2-2h15a2 2 0 0 1 2 2v9a2 2 0 0 1-2 2h-15a2 2 0 0 1-2-2z"></path>
                      <path id="tic5b" d="M496 530c0-1.1.9-2 2-2h7a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2h-7a2 2 0 0 1-2-2z"></path>
                      <path id="tic5c" d="M497 519c0-1.1.9-2 2-2h5a2 2 0 0 1 2 2v1a2 2 0 0 1-2 2h-5a2 2 0 0 1-2-2z"></path>
                      <path id="tic5e" d="M505 524a1 1 0 1 1 2 0 1 1 0 0 1-2 0z"></path>
                      <clipPath id="tic5d">
                        <use fill="#fff" xlink:href="#tic5a"></use>
                      </clipPath>
                      <clipPath id="tic5f">
                        <use fill="#fff" xlink:href="#tic5b"></use>
                      </clipPath>
                      <clipPath id="tic5g">
                        <use fill="#fff" xlink:href="#tic5c"></use>
                      </clipPath>
                    </defs>
                    <g>
                      <g transform="translate(-492 -517)">
                        <g>
                          <use fill="#fff" fill-opacity="0" stroke="#979797" stroke-miterlimit="50" stroke-width="4" clip-path="url(&quot;#tic5d&quot;)" xlink:href="#tic5a"></use>
                        </g>
                        <g>
                          <use fill="#979797" xlink:href="#tic5e"></use>
                        </g>
                        <g>
                          <use fill="#fff" xlink:href="#tic5b"></use>
                          <use fill="#fff" fill-opacity="0" stroke="#979797" stroke-miterlimit="50" stroke-width="4" clip-path="url(&quot;#tic5f&quot;)" xlink:href="#tic5b"></use>
                        </g>
                        <g>
                          <use fill="#fff" xlink:href="#tic5c"></use>
                          <use fill="#fff" fill-opacity="0" stroke="#979797" stroke-miterlimit="50" stroke-width="4" clip-path="url(&quot;#tic5g&quot;)" xlink:href="#tic5c"></use>
                        </g>
                      </g>
                    </g>
                  </svg>Распечатать</a>
          </div>
        </div>
      </div>
      <?
      $x++;
    }
    ?>
  </div>
  <div class="row">
    <div class="col-12 text-center">
      <!-- <button class="btn btn-red">Загрузить еще</button> -->
      <ul class="paginator">
        <li><a href="#!" class="prev">Предыдущая</a></li>
        <li><a href="#!">1</a></li>
        <li><a href="#!">2</a></li>
        <li><a href="#!" class="active">3</a></li>
        <li><a href="#!">4</a></li>
        <li><a href="#!">5</a></li>
        <li><a href="#!" class="next">Следующая</a></li>
      </ul>
    </div>
  </div>
</div>

<? include_once('_footer.php')?>
