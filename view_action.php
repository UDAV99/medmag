<? include_once('_header.php')?>

<div class="container">
  <div class="breadcrumbs">
    <ul>
      <li><a href="/">Главная</a></li>
      <li><a href="actions.php">Акции</a></li>
      <li>Акция "Скидки осени"</li>
    </ul>
  </div>
</div>

<h1 class="pagetitle text-center">Акция "Скидки осени"</h1>

<div class="container">
  <div class="row">
    <div class="offset-xl-1 col-xl-10">
      <div class="row">
        <div class="offset-xl-1 col-xl-10">
          <p>Идейные соображения высшего порядка, а также консультация с широким активом играет важную роль в формировании существенных финансовых и административных условий. Не следует, однако забывать, что новая модель организационной деятельности играет важную роль в формировании модели развития. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности представляет собой интересный эксперимент проверки соответствующий условий активизации.</p>
        </div>

        <p><img src="img/temp-action.jpg" class="img-fluid" alt=""></p>

        <div class="offset-xl-1 col-xl-10">
          <p>Таким образом новая модель организационной деятельности позволяет выполнять важные задания по разработке новых предложений. Задача организации, в особенности же укрепление и развитие структуры представляет собой интересный эксперимент проверки модели развития. Значимость этих проблем настолько очевидна, что новая модель организационной деятельности требуют от нас анализа новых предложений.</p>
          <p>Равным образом рамки и место обучения кадров требуют определения и уточнения существенных финансовых и административных условий. Не следует, однако забывать, что начало повседневной работы по формированию позиции играет важную роль в формировании соответствующий условий активизации. Задача организации, в особенности же рамки и место обучения кадров представляет собой интересный эксперимент проверки дальнейших направлений развития.</p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="inner_action_product">
  <div class="h2">Акционные товары</div>

  <div class="container">
    <div class="row refresh">
      <?
      $x=0;
      while ($x<4) {
        ?>
        <div class="col-sm-3 col-xl-3 product_cart">
          <div class="product_cart__box">
            <div class="product_cart__action">Скидка 60%</div>
            <div class="product_cart__img"><a href="#!"><img src="img/temp-product.png" alt="" class="img-fluid"></a></div>
            <div class="product_cart__title"><a href="#!">Биохимический анализатор Изи Тач (EasyTouch GCHb)</a></div>
            <div class="product_cart__price red">2540</div>
            <div class="product_cart__price--old">3870</div>
            <div class="product_cart__button"><a href="product.php" class="btn btn-red btn-sm"><span>Купить</span></a></div>
          </div>
        </div>
        <?
        $x++;
      }
      ?>
    </div>
  </div>
</div>

<? include_once('_footer.php')?>
