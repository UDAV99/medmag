<? include_once('_header.php')?>

<div class="container">
  <div class="breadcrumbs">
    <ul>
      <li><a href="/">Главная</a></li>
      <li>Контакты</li>
    </ul>
  </div>
</div>

<h1 class="pagetitle text-center">Контакты</h1>

<div class="container contacts">
  <div class="row">
    <div class="order-2 order-xl-1 col-12 col-xl-6">
      <div class="white_box">
        <div class="h2">Обратная связь</div>
        <form class="form" action="" method="post">
          <input type="text" name="name" placeholder="Ваше имя">
          <input type="tel" name="phone" placeholder="Контактный телефон">
          <textarea name="message" rows="8" placeholder="Комментарий"></textarea>
          <button class="btn btn-sm btn-red">Отправить</button>
        </form>
      </div>
    </div>
    <div class="order-1 order-xl-2 col-12 col-xl-6 mb30">
      <div class="white_box">
        <div class="h2">Контактные данные</div>
        <div class="row-phone"><a href="tel:+79262191949">8 (926) 219-19-49</a></div>
        <div class="row-mail"><a href="mailto:shop@site.ru">shop@site.ru</a></div>
        <div class="row-place">Россия, Москва</div>
      </div>
    </div>
  </div>
  <div class="row row-map">
    <div class="col-12">
      <div id="ya_addr"> </div>
    </div>
  </div>
</div>


<script type="text/javascript" src="http://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
<script type="text/javascript">
  // <![CDATA[
  ymaps.ready(function() {
    var myMap = new ymaps.Map('ya_addr', {
        center: [55.77729206897245,37.58534099999998],
        zoom: 16
      }, {
        searchControlProvider: 'yandex#search'
      }),
      myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
        hintContent: 'MedMag.ru',
        balloonContent: 'Россия, Москва, улица Бутырский Вал, 4'
      }, {
        iconLayout: 'default#image',
        iconImageHref: 'img/location.png',
        iconImageSize: [40, 50],
        iconImageOffset: [-20, -50]
      });

    myMap.geoObjects.add(myPlacemark);
    myMap.behaviors.disable('scrollZoom');
  });
  // ]]>
</script>


<? include_once('_footer.php')?>
